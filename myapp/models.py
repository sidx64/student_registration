from django.db import models
from django.conf import settings

# Create your modelss here.
class Login(models.Model):
    idlogin = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=15, blank=True, null=True)
    pass_field = models.CharField(db_column='pass', max_length=10, blank=True, null=True)
    status = models.CharField(max_length=45, blank=True, null=True)

class studentinfo(models.Model):
    DATE_INPUT_FORMATS = ['%m/%d/%Y']
    auto_increment_id = models.AutoField(primary_key=True)
    regno = models.CharField(max_length=40,blank=True,null=True)
    name = models.CharField(max_length=100,blank=True,null=True)
    sex = models.CharField(max_length=1,blank=True,null=True)
    per_add=models.CharField(max_length=1000,blank=True,null=True)
    pre_add=models.CharField(max_length=1000,blank=True,null=True)
    stud_mobile=models.CharField(max_length=10,blank=True,null=True)
    Father_mobile=models.CharField(max_length=10,blank=True,null=True)
    email=models.CharField(max_length=100,blank=True,null=True)
    caste = models.CharField(max_length=50,blank=True,null=True)
    degree_title = models.CharField(max_length=4,blank=True,null=True)
    univ_name = models.CharField(max_length=100,blank=True,null=True)
    agre_per = models.DecimalField(max_digits=5, decimal_places=2,blank=True,null=True)
    state = models.CharField(max_length=50,blank=True,null=True)
    test_attn=models.CharField(max_length=10,blank=True,null=True)
    rank_cet=models.IntegerField(null=True)
    rank_kmat=models.IntegerField(null=True)
    per_kmat = models.DecimalField(max_digits=5, decimal_places=2,blank=True,null=True)
    date_of_birth = models.DateField(settings.DATE_INPUT_FORMATS)
    quota=models.CharField(max_length=30,blank=True,null=True)
    pwd=models.CharField(max_length=2,blank=True,null=True)
    seatno=models.IntegerField(null=True)
    religion = models.CharField(max_length=50,blank=True,null=True)
    fname=models.CharField(max_length=100,blank=True,null=True)

