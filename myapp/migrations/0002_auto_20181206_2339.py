# Generated by Django 2.1.2 on 2018-12-06 18:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentinfo',
            name='pwd',
            field=models.CharField(blank=True, max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='studentinfo',
            name='quota',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='studentinfo',
            name='seatno',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='studentinfo',
            name='test_attn',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
