from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.http import HttpResponse
from myapp.models import studentinfo

# Create your views here.
class HomePageView(TemplateView):
    def get(self,request,**kwargs):
        return render(request, 'index.html',{'name':'RVCE-MCA'})

class LoginPageView(TemplateView):
    def get(self,request,**kwargs):
        return render(request,'login.html')
    
class ResultPageView(TemplateView):
    def get(self,request,**kwargs):
        un=request.GET['un']
        pas=request.GET['pas']
        if un=='rvce' and pas=='rvce':
            status='pass'
        else:
            status='fail'
        emplist=['empno1','empno2','empno3']
        return render(request,'result.html',{'un':un,'pas':pas,'status':status,'empno':emplist})

class ViewRegistration(TemplateView):
    def get(self, request,**kwargs):
        return render(request, 'registration.html',{'name':'Department of MCA.'})
    
class RegistrationSubmit(TemplateView):
    def get(self,request,**kwargs):
        reg=request.GET['regno']
        name=request.GET['name']
        mob=request.GET['mobileno']
        fmob=request.GET['fmobileno']
        fname=request.GET['fname']
        email=request.GET['email']
        dob=request.GET['dob']
        gender=request.GET['gender']
        padd=request.GET['paddress']
        pradd=request.GET['preaddress']
        location=request.GET['location']
        caste=request.GET['caste']
        uni=request.GET['university']
        agre=request.GET['agrepre']
        ent=request.GET['entrance']
        rank=request.GET['rank']
        deg=request.GET['degree'] 
        pwd=request.GET['pwd']
        quota=request.GET['quota']
        seatno=request.GET['seatno']
        reli=request.GET['religion']
        if(ent=="pgcet"):
            s=studentinfo(fname=fname,religion=reli,pwd=pwd,quota=quota,seatno=seatno,regno=reg,name=name,sex=gender,per_add=padd,pre_add=pradd,stud_mobile=mob,test_attn=ent,parent_mobile=fmob,email=email,caste=caste,agre_per=agre,univ_name=uni,state=location,degree_title=deg,rank_cet=rank,date_of_birth=dob)
        elif (ent=="kmat"):
            s=studentinfo(fname=fname,religion=reli,pwd=pwd,quota=quota,seatno=seatno,regno=reg,name=name,sex=gender,per_add=padd,pre_add=pradd,stud_mobile=mob,test_attn=ent,parent_mobile=fmob,email=email,caste=caste,agre_per=agre,univ_name=uni,state=location,degree_title=deg,rank_kmat=rank,date_of_birth=dob)
        s.save()
        return HttpResponse("<html><body>Record Inserted successfully!!!!</body></html>")
        #return DisplaySubmit.as_view()(self.request)
        
class DisplaySubmit(TemplateView):
    def get(self, request, **kwargs):
        pt = studentinfo.objects.order_by('regno')[:10]
        return render(request, 'display.html',{'studinfo':pt})
        
